package rayguiutils

import (
	"github.com/gen2brain/raylib-go/raygui"
	"github.com/gen2brain/raylib-go/raylib"
)

type Stack struct {
	X, Y, Width, Height int32
	Margin              int32
	currentY            int32
}

func NewStack(X, Y, Width, Height, Margin int32) *Stack {
	stack := Stack{X, Y, Width, Height, Margin, 0}
	stack.Reset()
	return &stack
}

func (stack *Stack) Reset() {
	stack.currentY = stack.Y
}

func (stack *Stack) Label(text string) {
	raygui.Label(raylib.NewRectangle(stack.X, stack.currentY, stack.Width, stack.Height), text)
	stack.currentY += stack.Height + stack.Margin
}

func (stack *Stack) Button(text string) bool {
	result := raygui.Button(raylib.NewRectangle(stack.X, stack.currentY, stack.Width, stack.Height), text)
	stack.currentY += stack.Height + stack.Margin
	return result
}

func (stack *Stack) CheckBox(text string, checkBoxSize int32, checked bool) bool {
	raygui.Label(raylib.NewRectangle(stack.X, stack.currentY, stack.Width-(checkBoxSize+4), stack.Height), text)
	result := raygui.CheckBox(raylib.NewRectangle(stack.X+stack.Width-(checkBoxSize+4), stack.currentY+(stack.Height-checkBoxSize)/2, checkBoxSize, checkBoxSize), checked)
	stack.currentY += stack.Height + stack.Margin
	return result
}

func (stack *Stack) ToggleGroup(texts []string, active int) int {
	result := raygui.ToggleGroup(raylib.NewRectangle(stack.X, stack.currentY, stack.Width, stack.Height), texts, active)
	stack.currentY += stack.Height + stack.Margin
	return result
}

func (stack *Stack) Spinner(value, min, max int) int {
	result := raygui.Spinner(raylib.NewRectangle(stack.X, stack.currentY, stack.Width, stack.Height), value, min, max)
	stack.currentY += stack.Height + stack.Margin
	return result
}

func (stack *Stack) Slider(value, min, max float32) float32 {
	result := raygui.Slider(raylib.NewRectangle(stack.X, stack.currentY, stack.Width, stack.Height), value, min, max)
	stack.currentY += stack.Height + stack.Margin
	return result
}

func (stack *Stack) Space(spaces int32) {
	stack.currentY += stack.Margin * spaces
}
